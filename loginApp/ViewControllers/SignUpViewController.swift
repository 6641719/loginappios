//
//  signUpViewController.swift
//  loginApp
//
//  Created by Amir on 05.04.2022.
//

import UIKit

class SignUpViewController: UIViewController {
    let scrollView = UIScrollView()
    let contentView = UIView()
    
    private let logoNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Stack signal"
        label.font = UIFont(name: "Poppins-Regular", size: 18)
        label.font = label.font.withSize(18)
        label.textColor = UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 0.3)
        return label
    }()
    
    private let welcomeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Welcome,\n go sign up!"
        label.font = UIFont(name: "Poppins-Regular", size: 18)
        label.font = label.font.withSize(36)
        label.textColor = UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 1.0)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    
    
    private var stackView: CustomStackTextField = {
        let stackView = CustomStackTextField()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let ageCheckView: CheckBox = {
        let button = CheckBox()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let signUpButton: UIButton = {
        var config = UIButton.Configuration.plain()
        config.contentInsets = NSDirectionalEdgeInsets(top: 21, leading: 16, bottom: 21, trailing: 16)
        var attText = AttributedString.init("Sign up")
        attText.font = UIFont(name: "Poppins-Regular", size: 16)
        config.attributedTitle = attText
        
        let button = UIButton(configuration: config)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .black
        button.layer.cornerRadius = 30
        button.backgroundColor = UIColor(red: 0.53, green: 0.89, blue: 0.91, alpha: 1.00)
        
        return button
    }()
    
    private let signInButton: UIButton = {
        var config = UIButton.Configuration.plain()
        config.contentInsets = NSDirectionalEdgeInsets(top: 20, leading: 0, bottom: 0, trailing: 0)

        var attText = AttributedString.init("Sign in / i already have an account")
        attText.font = UIFont(name: "Poppins-Regular", size: 14)
        config.attributedTitle = attText
        
        let button = UIButton(configuration: config)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .white
        button.addTarget(self, action: #selector(didTapSignInButton), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround() 
        view.backgroundColor = UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.00)
        setupView()
    }
    
    @objc private func didTapSignInButton() {
        let signInVC = SignInViewController()
        signInVC.modalPresentationStyle = .fullScreen
        present(signInVC, animated: true, completion: nil)
    }
    
    private func setupView() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        contentView.addSubview(stackView)
        contentView.addSubview(logoNameLabel)
        contentView.addSubview(welcomeLabel)
        contentView.addSubview(ageCheckView)
        contentView.addSubview(signUpButton)
        contentView.addSubview(signInButton)
        
        NSLayoutConstraint.activate([
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
                
            
            logoNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            logoNameLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            
            welcomeLabel.topAnchor.constraint(equalTo: logoNameLabel.bottomAnchor, constant: 24),
            welcomeLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            
            stackView.topAnchor.constraint(equalTo: welcomeLabel.bottomAnchor, constant: 96),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            contentView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor, constant: 16),
            
            ageCheckView.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 27),
            ageCheckView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            contentView.trailingAnchor.constraint(equalTo: ageCheckView.trailingAnchor, constant: 16),
            
            signUpButton.topAnchor.constraint(equalTo: ageCheckView.bottomAnchor, constant: 65),
            signUpButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            contentView.trailingAnchor.constraint(equalTo: signUpButton.trailingAnchor, constant: 16),
            
            signInButton.topAnchor.constraint(equalTo: signUpButton.bottomAnchor, constant: 20),
            signInButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            contentView.bottomAnchor.constraint(equalTo: signInButton.bottomAnchor, constant: 20)
        ])
    }
}


extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
