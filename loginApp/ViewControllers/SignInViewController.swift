//
//  SignInViewController.swift
//  loginApp
//
//  Created by Amir on 07.04.2022.
//

import UIKit

class SignInViewController: UIViewController {

    private let logoNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Stack signal"
        label.font = UIFont(name: "Poppins-Regular", size: 18)
        label.font = label.font.withSize(18)
        label.textColor = UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 0.3)
        return label
    }()
    
    private let welcomeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Welcome,\n go sign in!"
        label.font = UIFont(name: "Poppins-Regular", size: 18)
        label.font = label.font.withSize(36)
        label.textColor = UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 1.0)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    
    private let emailTF: CustomTextField = {
        var email = CustomTextField()
        email.translatesAutoresizingMaskIntoConstraints = false
        email.attributedPlaceholder = NSAttributedString(string: "E-mail", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white.withAlphaComponent(0.6)])
        return email
    }()
    
    private let passwordTF: CustomPasswordTextField = {
        let password = CustomPasswordTextField()
        password.translatesAutoresizingMaskIntoConstraints = false
        password.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white.withAlphaComponent(0.6)])
        return password
    }()
    
    private lazy var stackView: UIStackView = {
            let stackView = UIStackView(arrangedSubviews: [emailTF, passwordTF])
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.axis = NSLayoutConstraint.Axis.vertical
            stackView.spacing = 12.0
            stackView.distribution = .fillEqually
            stackView.layer.cornerRadius = 10
            stackView.isUserInteractionEnabled = true
            return stackView
        }()
    
    private let forgotPasswordButton: UIButton = {
        var config = UIButton.Configuration.plain()
        config.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0)
        var attText = AttributedString.init("Forgot Password")
        attText.font = UIFont(name: "Poppins-Regular", size: 14)
        config.attributedTitle = attText
        
        let button = UIButton(configuration: config)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .white
        return button
    }()
    
    private let signInButton: UIButton = {
        var config = UIButton.Configuration.plain()
        config.contentInsets = NSDirectionalEdgeInsets(top: 21, leading: 16, bottom: 21, trailing: 16)
        var attText = AttributedString.init("Sign in")
        attText.font = UIFont(name: "Poppins-Regular", size: 16)
        config.attributedTitle = attText
        
        let button = UIButton(configuration: config)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .black
        button.layer.cornerRadius = 30
        button.backgroundColor = UIColor(red: 0.53, green: 0.89, blue: 0.91, alpha: 1.00)
        
        return button
    }()
    
    private let signUpButton: UIButton = {
        var config = UIButton.Configuration.plain()
        config.contentInsets = NSDirectionalEdgeInsets(top: 20, leading: 0, bottom: 0, trailing: 0)

        var attText = AttributedString.init("New user?- Sign up")
        attText.font = UIFont(name: "Poppins-Regular", size: 14)
        config.attributedTitle = attText
        
        let button = UIButton(configuration: config)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .white
        button.addTarget(self, action: #selector(didTapSignUpButton), for: .touchUpInside)
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround() 
        view.backgroundColor = UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.00)
        setupView()
    }
    
    @objc private func didTapSignUpButton() {
       dismiss(animated: true, completion: nil)
    }
    
    private func setupView() {
        view.addSubview(logoNameLabel)
        view.addSubview(welcomeLabel)
        view.addSubview(stackView)
        view.addSubview(forgotPasswordButton)
        view.addSubview(signInButton)
        view.addSubview(signUpButton)
        
        NSLayoutConstraint.activate([
            logoNameLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
            logoNameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            welcomeLabel.topAnchor.constraint(equalTo: logoNameLabel.bottomAnchor, constant: 24),
            welcomeLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            signUpButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            signUpButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            view.safeAreaLayoutGuide.bottomAnchor.constraint(equalTo: signUpButton.bottomAnchor, constant: 22),
            signUpButton.topAnchor.constraint(equalTo: signInButton.bottomAnchor, constant: 20),
            
            signInButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            view.safeAreaLayoutGuide.trailingAnchor.constraint(equalTo: signInButton.trailingAnchor),
            signInButton.topAnchor.constraint(equalTo: forgotPasswordButton.bottomAnchor, constant: 60),
            
            view.safeAreaLayoutGuide.trailingAnchor.constraint(equalTo: forgotPasswordButton.trailingAnchor, constant: 16),
            forgotPasswordButton.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 12),
            
            stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            view.safeAreaLayoutGuide.trailingAnchor.constraint(equalTo: stackView.trailingAnchor)
            
        ])
    }
    
}
