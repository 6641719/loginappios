//
//  CustomStackTextField.swift
//  loginApp
//
//  Created by Amir on 06.04.2022.
//

import Foundation
import UIKit

class CustomStackTextField: UIView {
    
    private let eyeButton: UIButton = {
        var config =  UIButton.Configuration.plain()
        config.image = UIImage(systemName: "eye", withConfiguration: UIImage.SymbolConfiguration(scale: .large))
        let button = UIButton(configuration: config, primaryAction: nil)
        return button
    }()
    
    private let emailTF: CustomTextField = {
        var email = CustomTextField()
        email.translatesAutoresizingMaskIntoConstraints = false
        email.attributedPlaceholder = NSAttributedString(string: "E-mail", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white.withAlphaComponent(0.6)])
        return email
    }()
    
    private let nameTF: CustomTextField = {
        let name = CustomTextField()
        name.translatesAutoresizingMaskIntoConstraints = false
        name.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white.withAlphaComponent(0.6)])
        return name
    }()
    
    private let passwordTF: CustomPasswordTextField = {
        let password = CustomPasswordTextField()
        password.translatesAutoresizingMaskIntoConstraints = false
        password.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white.withAlphaComponent(0.6)])
        return password
    }()
    
    private let repeatPasswordTF: CustomPasswordTextField = {
        let repeatPassword = CustomPasswordTextField()
        repeatPassword.translatesAutoresizingMaskIntoConstraints = false
        repeatPassword.attributedPlaceholder = NSAttributedString(string: "Repeat password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white.withAlphaComponent(0.6)])
        return repeatPassword
    }()
    
    
    private lazy var stackView: UIStackView = {
            let stackView = UIStackView(arrangedSubviews: [emailTF, passwordTF, repeatPasswordTF, nameTF])
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.axis = NSLayoutConstraint.Axis.vertical
            stackView.spacing = 12.0
            stackView.distribution = .fillEqually
            stackView.layer.cornerRadius = 10
            stackView.isUserInteractionEnabled = true
            return stackView
        }()
    
    override init(frame: CGRect) {
           super.init(frame: frame)
           setupView()
       }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            trailingAnchor.constraint(equalTo: stackView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            stackView.topAnchor.constraint(equalTo: topAnchor),
        ])
    }
}
