//
//  CustomTextField.swift
//  loginApp
//
//  Created by Amir on 06.04.2022.
//

import Foundation
import UIKit


class CustomPasswordTextField: UITextField {
    private lazy var eyeButton: UIButton = {
        var config =  UIButton.Configuration.plain()
        let button = UIButton()
        button.configuration = config
        button.configurationUpdateHandler = { [unowned self] button in
          var config = button.configuration
          let colorSelected = UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 1.00)
          config?.image = self.checkButton
            ? UIImage(systemName: "eye.slash", withConfiguration: UIImage.SymbolConfiguration(scale: .large))?.withTintColor(colorSelected, renderingMode: .alwaysOriginal)
            : UIImage(systemName: "eye", withConfiguration: UIImage.SymbolConfiguration(scale: .large))?.withTintColor(colorSelected, renderingMode: .alwaysOriginal)
            self.isSecureTextEntry = !self.checkButton
          button.configuration = config
          button.addAction(
              UIAction { _ in
                  self.checkButton = !self.checkButton
              },
              for: .touchUpInside
            )
        }
        return button
    }()
    
    private var checkButton = false {
      didSet {
        eyeButton.setNeedsUpdateConfiguration()
      }
    }
    
    private let insets = UIEdgeInsets(top: 21, left: 21, bottom: 21, right: 21)
    
    init(){
        super.init(frame: .zero)
        textColor = UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 1.00)
        layer.cornerRadius = 20
        layer.borderWidth = 2
        font = UIFont(name: "Poppins-Regular", size: 14)
        backgroundColor = UIColor(red: 0.07, green: 0.07, blue: 0.07, alpha: 1.00)
        delegate = self
        rightView = eyeButton
        rightViewMode = .unlessEditing
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CustomPasswordTextField {
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: insets)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: insets)
    }
}

extension CustomPasswordTextField: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        layer.borderColor = UIColor(red: 0.53, green: 0.89, blue: 0.91, alpha: 1.00).cgColor
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        layer.borderColor = UIColor(red: 0.07, green: 0.07, blue: 0.07, alpha: 1.00).cgColor
    }
}


