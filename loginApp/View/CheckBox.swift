//
//  CheckBox.swift
//  loginApp
//
//  Created by Amir on 06.04.2022.
//

import UIKit

class CheckBox: UIButton {
    private lazy var buttonWithImage: UIButton = {
        var config =  UIButton.Configuration.plain()
        
        var attText = AttributedString.init("Accept that i'm 18 or older")
        attText.font = UIFont(name: "Poppins-Regular", size: 16)
        config.attributedTitle = attText
        config.imagePadding = 10
        
        var button = UIButton()
        button.configuration = config
        button.configurationUpdateHandler = { [unowned self] button in
          var config = button.configuration
          let colorSelected = UIColor(red: 0.53, green: 0.89, blue: 0.91, alpha: 1.00)
          config?.image = self.checkButton
            ? UIImage(systemName: "checkmark.square.fill", withConfiguration: UIImage.SymbolConfiguration(scale: .large))?.withTintColor(colorSelected, renderingMode: .alwaysOriginal)
            : UIImage(systemName: "square", withConfiguration: UIImage.SymbolConfiguration(scale: .large))
          button.configuration = config
        }
        button.translatesAutoresizingMaskIntoConstraints = false
        button.contentHorizontalAlignment = .left
        button.tintColor = .white
        button.addAction(
          UIAction { _ in
              self.checkButton = !self.checkButton
          },
          for: .touchUpInside
        )
        return button
    }()
    
    private var checkButton = false {
      didSet {
        buttonWithImage.setNeedsUpdateConfiguration()
      }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(buttonWithImage)
        
        NSLayoutConstraint.activate([
            buttonWithImage.topAnchor.constraint(equalTo: topAnchor),
            buttonWithImage.leadingAnchor.constraint(equalTo: leadingAnchor),
            buttonWithImage.trailingAnchor.constraint(equalTo: trailingAnchor),
            buttonWithImage.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            buttonWithImage.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
}
