import Foundation
import UIKit


class CustomTextField: UITextField {
    
    let insets = UIEdgeInsets(top: 21, left: 21, bottom: 21, right: 21)
    
    init(){
        super.init(frame: .zero)
        textColor = UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 1.00)
        layer.cornerRadius = 20
        layer.borderWidth = 2
        font = UIFont(name: "Poppins-Regular", size: 14)
        backgroundColor = UIColor(red: 0.07, green: 0.07, blue: 0.07, alpha: 1.00)
        delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CustomTextField {
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: insets)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: insets)
    }
}

extension CustomTextField: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        layer.borderColor = UIColor(red: 0.53, green: 0.89, blue: 0.91, alpha: 1.00).cgColor
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        layer.borderColor = UIColor(red: 0.07, green: 0.07, blue: 0.07, alpha: 1.00).cgColor
    }
}
